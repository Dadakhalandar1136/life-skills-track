# Focus Management

## **Question 1**

### What is Deep Work?

According to Cal Newport Deep work is termed as focusing without any distraction on a cognitively demanding task.

* Coding is the classic example of deep work because we got this problem, we need to solve, we have all of these tools and we have to combine them all creatively. There it needs more focus.

## **Question 2**

### Paraphrase all the ideas in the above videos and this one **in detail.**

According to Cal Newport, the Optimal duration for deep work is at least 1 hour.

Example: When we are coding for 20mins we might only get 10 or 15 minutes of actual work, so at least 1 hour is the optimal duration for deep work. If work deeply for 1 hour we might get 45 to 50 minutes of work.

Deadlines are a clear motivational signal but in the short term, we do get an effect like that in time blocking. The strong effect we get by saying this is the exact time I'm going to work on this is that we don't have the debate with ourselves every three minutes about taking breaks. So if we have deadlines we will spend all the time completing the particular task. So deadlines are good for productivity.

Three deep work strategies that we can incorporate into our schedule to heighten our ability to focus and produce results that are hard to replicate.

1. Schedule Distractions: 

Schedule distraction periods at home and at work. Most of us allow ourselves to go online at any moment and check our phone whenever buzzes or dings but doing so is training our brains to avoid deep work. To overcome this distraction, put a notepad nearby and put down the next distraction break we will have to hold our focus until that time.

2. Deep Work Ritual:

Developing a deep work ritual is the easiest way to consistently start deep work sessions to transform them into a simple regular habit.

3. Evening Shutdown:

The third strategy to cultivate deep work in our life is to have a daily shutdown complete ritual. Sleep is the price we need to pay in order to do deep work.

## **Question 3**

### How can you implement the principles in your day-to-day life?

I can implement the principles in my day-to-day life by following these steps:

* Follow the three Deep Work strategies.
* Deep Work for at least one hour.
* Schedule the distractions by doing certain things like keeping mobile in silent mode.
* Follow the Evening Shutdown strategy.
* Sleep for sufficient time.

## **Question 4**

### Your key takeaways from the video.

Some key points of the video :

* Without social media, we still make friends.
* Without social media, we can still know what's going on in the world.
* Without social media, we can be better.
* Social media seems to be a little bit dangerous.
* Social Media wastes too much time and is unhealthy for the mind and body.
* Social Media keeps distracting as it is designed to keep your attention for as long as possible.
* Use of social media is bad for one's mental health as it leads to the comparison of lifestyles which can depress a person.