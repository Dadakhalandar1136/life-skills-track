# **Grit and Growth Mindset**

## **1. Grit**<br>

![image](https://www.studyallknight.com/wp-content/uploads/2019/09/GRIT-Stands-For-Growth-Mindset-Teachers-Promoting-Grit-Classroom-Activities-Lessons-Blog-Post-1500x635.png)

<br>

### **Question 1**

#### **Video Summery**

Angela Lee Duckworth explains that in life a significant predictor of success is "grit" or "passion and perseverance for very long-term goals."

### **Question 2**

#### **Takeaways**

* Ability to learn is not fixed it can change with our efforts.
* The belief in our capacity to learn and grow.
* We should start learning from the mistakes which we make.
* Should have a mindset that we can and we will.
* Should change the way of learning.

## **2. Introduction to Growth Mindset**

### **Question 3**

#### **Video Summury**

**There are two types of mindsets**

* **Fixed Mindset:**

They believe that skills and intelligence are set and you either have them or you don't. some people are just naturally good at things, while others are not.

* **Growth Mindset:**

They believe that skills and intelligence are grown and developed. people who are good at something are good. After all, they built that ability, and people who aren't - are not good because they haven't done the work.

A growth mindset is like whatever we can get in our life we still feel like we can improve. Believe and focus to create a growth mindset.

![imageforgrowthmindset](https://cdn.shortpixel.ai/spai/w_973+q_glossy+ret_img+to_webp/https://www.teachertoolkit.co.uk/wp-content/uploads/2016/03/shutterstock_352900961-820x545.jpg)

### **Question 4** 

#### **Takeaways**

* We should believe that we can do whatever task we face in our life.
* We should focus on what we are doing at present.
* We should positively receive feedback.
* We should always try to learn new things.
* We should learn from our mistakes.

## **3. Understanding Internal Locus of control**

### **Question 5**

**What is the Internal Locus of Control ?**

Internal locus of control can also be called "agency". Overall, it incorporates the ability to take action, be effective, influence your own life, and assume responsibility for your behaviors. Individuals with a high internal locus of control believe that their interactions with their environment will produce predictable results (Li, Lepp, & Barkley, 2015).

Research shows that internal locus of control predicts better health outcomes, work satisfaction, and academic success. By contrast, someone with a strong external locus will ascribe their career failures or problems to others and NOT take corrective action.

**Key points from the video**

* The degree to which you believe you have control over your life."
* How much work you put into something is something that you have complete control over.
* Take time and complete the task but don't give up.
* Appreciate yourself when getting success because of my hard work I got success in my life.
* Build that believes you are in control of your destiny. 

## **3. How to build a Growth Mindset**

### **Question 6**

#### **Video Summury**

* Believe in your ability to figure out things.
* Question on you are assumptions.
* Develop your life curriculum.
* Honur the struggle.

### **Question 7**

#### **Takeaways**

* Identifying my mindset.
* I will look at my improvements.
* I will take review the success of others.
* Rather than scolding myself for my errors, I will try to identify how would I treat someone else in my situation.
* I will set realistic goals.

## **4. Mindset - A MountBlue Warrior Reference Manual**

### **Question 8**

* I will stay with a problem till I complete it. I will not quit the problem.
* I will understand each concept properly.
* I will stay relaxed and focused no matter what happens.
* I will wear confidence in my body. I will stand tall and sit straight.
* I will always be injustice. I will have a smile on my face when I face new challenges or meet people.
* I know more efforts lead to better understanding.