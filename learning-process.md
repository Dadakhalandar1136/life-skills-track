# **Learning Process**

## **1. How to Learn Faster with the Feynman Technique?**

### **Question 1**
**What is Feynman Technique?**

Feynman Technique states that if you understand a concept then you should be able to explain the same most thoroughly. This technique is a **four-step** process.
<br>

* Take A piece of paper and write the concept's name at the top.
* Explain the concept using simple language.
* Identify problem areas, then go back to the sources to review.
* Pinpoint any complicated terms and challenge yourself to simply them.

### **Question 2** 
**What are the different ways to implement this technique in your learning process ?**

* Take a paper and write the concept at the top
* Explain the concept most simply.
* Identify the shaky areas, if so go to the source and learn it.
* If any technical terms are involved check if you could explain that.

## **2. Learning How to Learn TED talk by Barbara Oakley**
### **Question 3** 
**Paraphrase the video in detail in your own words.**

* Learning how to learn is the most powerful technique.
* Our brain has 2 modes focused mode and diffuse mode.
* When getting stuck in learning relax and allow the diffused mode to work in the background.
* We can overcome procrastination using the Pomorodo technique.
* The Pomorodo technique states to try to focus for 25 minutes avoiding all distractions. After that do something fun and relaxing.
* Slow thinking is not bad, it helps to build a strong foundation

### **Question 4**
**What are some of the steps that you can take to improve your learning process ?**

* Practice and repetition help in the mastery of any concept.
* When getting stuck in learning relax for while thinking about the problem. This will help to find a loophole and then try working on it again.


## **3. Learn Anything in 20 hours**

### **Question 5**
**Your key takeaways from the video ?**

**Video Summary**
we can learn anything in 20 hrs if we follow these steps :
*  **Deconstruct the skill** - FiWhat are some of the steps that you can while approaching a new topic? figure out what we need to be able to do through learning the skill. Break the learning process into smaller tasks.
* **Learn enough to self-correct** - Take any 5 good resources and learn from them until we can correct ourselves.
* **Remove barriers in practice** - During learning avoid all distractions such as televisions, smartphones, internet. Stay focused.
* **Practice at least 20 hours** - People get demotivated and overwhelmed at the start of learning any skill. Once we get a hold of it we are good to go.

**Takeaway**
* We can achieve any new skills in just 20 hours.
* Stay focused while learning.
* Avoid distractions while learning.
* While starting off learning new skills we might get demotivated but at least provide 20 hours to find out if you could do it.

### **Question 6** 
**What are some of the steps that you can while approaching a new topic?**

* List down the topics. Study topic-wise only.
* Pick A good video tutorial.
* I will Recall once and Revise and I will make notes of it.
* I will Practice more and more.
