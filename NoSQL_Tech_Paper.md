# NoSQL?

## What is a NoSQL database?

- NoSQL databases are non-tabular databases.
- It stores data differently than relational tables.
- It has different types of data models which provide flexible schemas and scale easily with large amounts of data and high user loads.
- NoSQL database optimized for developer productivity.

## Types of NoSQL database models

**There are four major types of NoSQL databases emerged:**

* **Document databases**:- store data in documents similar to JSON (JavaScript Object Notation) objects. Each document contains pairs of fields and values. The values can typically be a variety of types including things like strings, numbers, booleans, arrays, or objects.
* **Key-value databases**:- are a simpler type of database where each item contains keys and values.
* **Wide-column stores**:- store data in tables, rows, and dynamic columns.
* **Graph databases**:- store data in nodes and edges. Nodes typically store information about people, places, and things, while edges store information about the relationships between the nodes.

To learn more, visit [Understanding the Different Types of NoSQL Databases.](https://www.mongodb.com/scale/types-of-nosql-databases)

## Why NoSQL?

NoSQL databases are used in nearly every industry. Use cases range from the highly critical (e.g., storing financial data and healthcare records) to the more fun and frivolous (e.g., storing IoT readings from a smart kitty litter box).

In the following sections, we’ll explore when you should choose to use a NoSQL database and common misconceptions about NoSQL databases.

## When should NoSQL be used?

When deciding which database to use, decision-makers typically find one or more of the following factors that lead them to select a NoSQL database:

* Fast-paced Agile development
* Storage of structured and semi-structured data
* Huge volumes of data
* Requirements for scale-out architecture
* Modern application paradigms like microservices and real-time streaming

See When to Use NoSQL Databases and Exploring NoSQL Database Examples for more detailed information on the reasons listed above.

## The 5 Best NoSQL databases

1. **MongoDB (Document database)** 

 MongoDB is an object-oriented, simple, dynamic, and scalable NoSQL database. It is based on the NoSQL document store model. The data objects are stored as separate documents inside a collection instead of storing the data in the columns and rows of a traditional relational database. MongoDB users JSON-like documents with schemas.

2. **Redis (Key-value database)**

 Redis is an open-source in-memory database project implementing a distributed, in-memory key-value store with optional durability. It can also be used as a cache and message broker. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyper logs, and geospatial indexes with radius queries.

3. **Apache Cassandra (Wide-column stores)**

 Apache Cassandra is a free and open-source distributed NoSQL database management system designed to handle large amounts of data across many commodity servers, providing high availability with no single point of failure.

4. **Neo4j (Best Graph Database)**

 Neo4j is not one of your ordinary NoSQL databases. It is a graph database that offers users a flexible data model that relational SQL databases do not offer. With Neo4j, you can have a data model as nodes and relationships. This makes it easy to represent complex data structures.

5. **Apache Hbase (Best for Big Data Applications)**

 If you're looking for a fast and scalable NoSQL database, Apache HBase is worth considering. It is the open-source version of Google's Big Table database. HBase is an open-source, columnar-oriented NoSQL database that runs on top of the Hadoop Distributed File System(HDFS).

## References

* [About NoSQL](https://www.mongodb.com/nosql-explained)

* [References for NoSQL databases]( https://medium.com/javarevisited/5-best-nosql-database-programmers-and-developers-can-learn-42a0bdfa9a12 )

* [Watch HERE](https://www.youtube.com/watch?v=B3gJT3t8g4Q)
