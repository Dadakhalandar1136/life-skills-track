# **Listening and Active Communication**
## **1. Active Listening**
The act of fully hearing and comprehending the meaning of what someone else is saying.<br><br>
### **1. Question<br> What are the steps/strategies to do Active Listening?**
* **Face the speaker and have eye contact**<br>
    Eye contact is an important part of face-to-face conversation. Too much eye contact can be intimidation, though, so adapt this to the situation you're in.<br>
* **"Listen" to non-verbal cues too**<br>
    Pay attention to what the other person is saying with their body language.<br>
* **Don't interrupt**<br>
    Being interrupted is frustrating for the other person - it gives the impression that you think you're more important, or that you don't have time for what they have to say.<br>
* **Don't start planning what to say next**<br>
    You can't listen and prepare at the same time.<br>
* **Show that you're listening**<br>
    Nod your head, smile, and make small noises like "yes" and "uh huh", to show that you're listening and encourage the speaker to continue. Don't look at your watch, fidget, or play with your hair or fingernails.<br>
* **Ask questions**<br>
    Asking relevant questions can show that you're been listening and help clarify what has been said.<br><br>
## **2. Reflective Listening**
 **Question 2**<br> According to Fisher's model, what are the key points of Reflective listening?<br>
 * We should listen more than talk.<br>
 * We should try to understand the feeling contained in what the other person is saying, not just the facts or ideas.<br>
 * We should respond with acceptance but not with fake concern.<br>
 * We should not respond with a cold vibe.<br>
 * we should restate and give clarify what others said.<br><br>
## **3. Reflection**

**Question 3**<br>
What are the obstacles in your listening process?<br>
* Distractions from my surroundings.<br>
* Prioritize listening over speaking.<br>
* Ask questions.<br>
* Listen fully before giving advice.<br>
* Reduce outside noise.

<br>**Question 4**<br>
What can you do to improve your listening?<br>
* Considering eye contact while talking.<br>
* Making a mental image of what the other person is talking about.<br>
* Providing feedback after a conversation.<br>
* Listening without jumping to the conclusion.<br>
* Being alert when another person is talking.<br><br>
## **4. Types of Communication**<br>
**Question 5**<br>
When do you switch to a Passive communication style in your day-to-day life?<br>
* Whenever I meet someone who is older than me.<br>
* whenever I make a mistake which I was not knowing that it is wrong.<br>
* When I was having one-to-one meetings.<br><br>

**Question 6**<br>
When do you switch to Aggressive communication styles in your day-to-day life?<br>
* When I get frustrated because of hunger.<br>
* When my brother suggests me something which I don't like.<br>
* when I don't get proper sleep.<br><br>

**Question 7**<br>
When do you switch to Passive Aggressive(sarcasm/gossiping/taunts/silent treatment and others)communication styles in your day-to-day life?<br>
* When I go somewhere with my friends.
* While I am have something in the hotel.
* When someone tries to test the other person without reason.<br><br>

**Question 8**<br>
How can you make your communication assertive?<br>
* Taking care of my tone while talking with someone.<br>
* Maintaining body language while explaining something to someone.<br>
* Making a script that what I want to say in the meetings.<br>
* I will not compromise my comfort for others.