# Tiny Habits

## 1. Tiny Habits - BJ Fogg

### Question 1 

#### Your takeaways from the video

* Design habits for the behavior to achieve the outcome, not the outcome itself.
* Have tiny habits which are designed to achieve an outcome. They take root in your system and compound the effect you put in to achieve the outcome

* To create a habit, one requires motivation, ability, and a trigger
* Triggers can also have a cascading behavior. Old behaviors can be used to trigger new behaviors
* After I enter existing habit, I will enter new tiny behavior

## 2. Tiny Habits by BJ Fogg - Core Message

### Question 2 

#### Your takeaways from the video in as much detail as possible

* Behaviour = Motivation + Ability + Prompt
* Shrink the behavior: Shrink habits to the tiniest possible version so that very little motivation is required to do it
  * Find a behavior that is easy to do in 30 seconds or less
  * Find the tiniest habit with the biggest impact
* Identify a trigger
  * External Triggers
    * Too easy to ignore
  * Internal Triggers
    * Too easy to ignore
  * Action Triggers
    * Use the completion of one behavior to trigger a new behavior
* Grow your habit with some Shine
  * Start with something tiny and nurture it
  * The habit will naturally grow into a bigger habit
  * Shine: Authentic Pride, Celebrate when you achieve those tine habits every time you complete it

### Question 3

How can you use B = MAP to make making new habits easier?

* Start with a small habit that requires low motivation to complete, and we have the high ability to do it. Have behaviors that trigger those habits.

### Question 4

Why it is important to "Shine" or Celebrate after each successful completion of a habit?

* A pat on the back is always a good motivation

## 3.1% Better Every Day Video

### Question 5 

#### Your takeaways from the video (Minimum 5 points)

* There are four stages of Habit Formation

  * Noticing
  * Wanting
  * Doing
  * Liking

* You can't take an action if you don't notice something
  * Implementation Action
    * Have the plan to implement the behavior
    * Having clarity, and motivation without clarity of plan does nothing
* If you notice something, you should be 'wanting' to take an action on it
  * Design your environment to drive you toward achieving your habit
    * You want to read a book before bed? Keep the book on the pillow.
  * Make bad habits harder to achieve and good habits easier to achieve
* Learning how to start doing things is important
  * Two-minute rule:
    * If it takes two minutes, do it now
    * Any habit can be started in less than two minutes
    * Optimise for the beginning of the task
  * Best way to change long-term behaviour is with short-term feedback
  * Don't break the chain and never miss twice
* Like what you do
* If you can change your habits, you can change your life

## 4. Book Summary of Atomic Habits

### Question 6 

#### Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.

* Don't focus on goals, focus on designing systems to achieve that goals
* The purpose of setting goals is to win the game. The purpose of designing systems us to continue playing the game

### Question 7 

#### Write about the book's perspective on how to make a good habit easier.

* Make it obvious, make it easy to trigger
* Keep fewer steps between you and the good behavior
* Make it immediately satisfying

### Question 8 

#### Write about the book's perspective on making a bad habit more difficult.

* Make it invisible, make it hard to trigger
* Keep more steps between you and the bad behavior
* Make it unsatisfying

## 5. Reflection

### Question 9 

#### Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

1. I will read a book every day before sleeping
    * I can make it easier by keeping the book I am reading on my bedside table
    * Reward me with sleep after 20 minutes of reading

### Question 10 

#### Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

1. Sleeping late
    * Keep all the devices and distractions away from the bed
