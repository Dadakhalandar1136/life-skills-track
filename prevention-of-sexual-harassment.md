# **Prevention of Sexual Harassment**

## **Question 1**
### **Behaviour that causes Sexual Harassment.**

Any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment.

**Sexual Harassment is divided into three forms.**

1. Verbal Sexual Harassment.
2. Visual Sexual Harassment.
3. Physical Sexual Harassment.

**Verbal Sexual Harassment**

* Comments about clothing.
* A person's body.
* Sexual or gender-based jokes or remarks.
* Requesting sexual favors. 
* Repeatedly asking a person out.
* As well as innuendos.
* Threats. 
* Spreading rumors about a person's personal or sexual life.
* Using foul and abusive language.
Comes under verbal sexual harassment.

**Visual Sexual Harassment**

* Visual Sexual Harassment includes abuse posters.
* Drawings.
* Pictures.
* Screen savers cartoons. 
* Emails or texts of a sexual nature.

**Physical Sexual Harassment**

* Physical Sexual Harassment includes sexual assault.
* Impeding or blocking movement. 
* Inappropriate touching such as kissing. 
* Hugging. 
* Patting. 
* Stroking or rubbing. 
* Sexual gesturing or leering or staring.

## **Question 2**
### **In case I face or witness any incident or repeated incidents of Sexual Harassment behavior.**

**Case:- I face such behaviour**

In the event that I am subjected to sexual harassment. 
* I will observe the behavior to determine whether the harasser is doing so knowingly or unknowingly. 
* If they are doing so unknowingly, I will warn them. 
* If they repeat the same behavior, I will inform the higher authorities about their behavior.
* If they are doing it knowingly, I will immediately inform higher authorities about their behavior.
* If they do not change their behavior meaningless of any warnings from me and higher authorities, then I will file a case against them under Sexual Harassment Act.

**Case:- I witness any sexual harassment behavior**

In the event that I witness sexual harassment, I will immediately support the victim and warn the harasser. If the harasser is my coworker, I will warn the harasser to change their behavior. If they do the same, I will inform the higher authorities. If the harasser is my superior, I will notify the appropriate authorities about his or her behavior. Regardless of any warnings, if they continue the same behavior, I will file a case against them under the Sexual Harassment Act.